from __future__ import unicode_literals

from django.db import models


class Category(models.Model):
	name = models.CharField(max_length=128, unique=True)

	def __unicode__(self):
		return self.name


class workspace(models.Model):
	DEFAULT_ID = 1
	category = models.ForeignKey(Category,default=DEFAULT_ID)
	name = models.CharField(max_length=128, null=True)
	address =  models.CharField(max_length=250, null=True)
	city = models.CharField(max_length=128, null=True)
	about = models.TextField(blank=True, null=True)
	
	HOUR12 = '12HR'
	HOUR24 = '24HR'

	select = (
		(HOUR12, '12 Hours'),
		(HOUR24, '24 hours'),
		)
	validity = models.CharField(max_length =20, choices = select, default = HOUR12)

	price = models.CharField(max_length = 10, null=True)
	rate = models.IntegerField(max_length = 1, default=5)
	photo = models.ImageField(upload_to = 'static/images/',blank=True, null=True)

	def __unicode__(self):
		return self.name
